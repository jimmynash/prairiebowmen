---
layout: post
title: Prairie Bowmen Archery Kid's Day Camp July 13, 2019
description: Kids Archery Day Camp
image:
updated:
---
Saturday July 13th, 9:00 a.m. - 5:30 p.m.

Take the kids out for a fun day of activities,crafts and archery.

Open to members and non-members.

<!--more-->

* Where: Outdoor Range, 6333 Maple View Drive
* Registration is now closed as the event is past.
* Shooting starts at 9:00 am
* Cost: $25.00 - includes lunch
* Equiment provided if needed
