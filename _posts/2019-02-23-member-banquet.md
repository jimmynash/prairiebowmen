---
layout: post
title: Member Banquet
description: Member Banquet
image:
updated:
---
The 2019 member banquet will take place on March 9, 2019 at the Highlands Golf Course at 5501 NW 12th Street, Lincoln, Nebraska.

We will start at 6:00 p.m. and go until 9:00 p.m..
