---
layout: post
title: We Need a New Indoor Range
description: Lease was lost. Gotta move.
image:
updated:

---
Prairie Bowmen Members,

The Prairie Bowmen Board was notified on Friday June 28th, that our lease at the indoor range will not be renewed. After being there for the past 30 years, this was a shock to us, but that is business these days. The auto shop on the west side of the building wants to expand and has offered double the rent.

With that being said, we will be looking for a new location. The sooner the better. Ideally we’d like a building 80 x 120, with a restroom and open floor space. So keep your eyes and ears open if you know of something out there and let me know. My email is jimtubbs@windstream.net and my phone is 402-430-7783. I believe we have until the end of September, which is not a lot of time.

In the meantime, it will be business as usual and the next event will be the Jamboree on August 17th & 18th, with the State Marked 3D on the 18th.

Jim Tubbs PBA President
