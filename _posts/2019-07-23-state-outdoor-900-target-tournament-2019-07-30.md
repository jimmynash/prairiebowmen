---
layout: post
title: Nebraska State Outdoor 900 Target Tournament
description: SUNDAY JUNE 30th, 2019
image:
updated:

---
## Nebraska State Outdoor 900 Target Tournament

### SUNDAY JUNE 30th, 2019
<!--more-->
* Where: Outdoor Range, 6333 Maple View Drive
* Registration at 8:00 am
* Shooting starts at 9:00 am
* Lunch available for purchase

#### FOR MORE INFO PRAIRE BOWMEN

Jim Tubbs (402) 430-7783 345 S. 14th St
pbarchery@gmail.com Lincoln, NE

_Outdoor range is closed June 29 in preparation for the 900 and June 30 for the shoot.
Your help getting the range ready on June 29 would be greatly appreciated. Thank you_
