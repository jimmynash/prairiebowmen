---
layout: post
title: WORK DAY AT OUTDOOR RANGE – JUNE 1ST AT 7:00 A.M.
description: WORK DAY AT OUTDOOR RANGE – JUNE 1ST AT 7:00 A.M.
image:
updated:
---
The work day originally scheduled for May 18th has been rescheduled for June 1st.

There will be a workday at the outdoor range on Saturday June 1st to get things ready for the June 3D.
<!--more-->

Our primary job will be getting the lanes ready for the 3D.   It would be greatly appreciated if everyone could come to help out.  If you have a chainsaw, weed eater, or any kind of brush trimmers please bring them.  We will start at 7:00 a.m. and go until at least Noon.   Donuts will be provided and lunch will be provided for those that come to help.

Any questions please call/text 402-499-0470 Brian Krogman Range Master
