---
layout: post
title: NEXT SESSION OF JOAD STARTS JULY 9
description: NEXT SESSION OF JOAD STARTS JULY 9
image:
updated:
---
The next session of youth instruction and practice sessions will start on July 9, 2019.

Please contact Steve Woitaszewski for more details.
