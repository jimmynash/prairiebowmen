---
layout: post
title: Work day at indoor range March 30 - 31
description: Work day at indoor range March 30 - 31
image:
updated:
---
Two work days have been scheduled at the indoor range for the purpose of rebuilding the indoor targets. This will happen on March 30 & 31.
<!--more-->

Everyone will benefit from this work so it would be nice if everyone could find the time to help. We will start at 7:00 a.m. each day and stop at noon each day.

DONUTS WILL BE PROVIDED!!!!!!

Please find some time to help with this project. You can see old friends and make new ones. The indoor range will be closed to shooting during the work days.
