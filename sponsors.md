---
layout: page
title: Sponsors
description: "Generous Sponsors of Prairie Bowmen Archery"
image:
nav-menu: true
show_tile: false
order: 3

thumb_base: "https://res.cloudinary.com/dgkbqzavd/image/upload/h_250,w_250,g_center,c_pad,b_rgb:242943/v1578971387/pba_media/sponsors/"
image_base: "https://res.cloudinary.com/dgkbqzavd/image/upload/v1577397129/pba_media/sponsors/"

sponsors:
  - { title: '84 Lumber', logo: '84_Lumber_Logo_Stacked_txmvss.jpg', url: 'https://www.84lumber.com/' }
  - { title: 'Big Game Association', logo: 'big-game-assoc-logo_ipfjht.png', url: 'http://biggameconservationassociation.com/' }
  - { title: 'Big Red Outdoors', logo: 'big_red_outdoors_logo_qamje6.jpg', url: 'https://www.bigredoutdoors.com/' }
  - { title: 'Blue River Archery', logo: 'blue_river_archery_ougk4y.jpg', url: 'https://www.facebook.com/Blue-River-Archery-LLC-128458110580225/' }
  - { title: 'Clear Creek Archery', logo: '', url: 'https://clearcreekarchery.com' }
  - { title: 'Davis Sights', logo: 'davissights_logo_dnckkc.png', url: 'http://www.davissights.com' }
  - { title: 'Glass Act Photography', logo: 'glass-act-photography-logo-white_cyehao.png', url: 'https://glassactphoto.com/' }
  - { title: 'Hyde Family Chiropractic', logo: 'hyde_family_chiro_logo_ihng8r.jpg', url: 'https://www.hydefamilychiropractic.com/' }
  - { title: 'Krogman Custom Woodworks', logo: '', url: '' }
  - { title: 'Nebraska Bowhunters Assoc.', logo: 'NBA_logo_ijusdg.jpg', url: 'https://www.nebraskabowhunters.com' }
  - { title: 'Nebraska Game and Parks.', logo: 'negameandparks_logo_pgtg9t.jpg', url: 'http://outdoornebraska.gov' }
  - { title: 'Scheels', logo: 'scheels_zlmmie.png', url: 'https://www.scheels.com' }
---
<div id="main" class="alt">
  <section id="one">
    <div class="inner">
      <header class="major">
        <h1>{{ page.title }}</h1>
        <p>Our generous sponsors.</p>
      </header>
      <div class="sponsors row">
        {% for sp in page.sponsors %}
          <article class="6u 12u$(medium) article special">
            <h2 class="title">{{ sp.title }}</h2>
            {% if sp.url != empty %}
            <a href="{{ sp.url }}" alt="{{ sp.title }}" title="{{ sp.title }}" target="_blank">
              {% if sp.logo != empty %}
                <img src="{{ page.thumb_base }}{{ sp.logo }}" alt="{{ sp.title }} Logo">
              {% else %}
              <i class="icon fa-users fa-5x"></i>
              {% endif %}
            </a>
            {% else %}
              <span>
                {% if sp.logo != empty %}
                <img src="{{ page.thumb_base }}{{ sp.logo }}" alt="{{ sp.title }} Logo">
                {% else %}
                <i class="icon fa-users fa-5x"></i>
                {% endif %}
                <i></i>
              </span>
            {% endif %}
              <hr>
          </article>
        {% endfor %}
      </div>
      <p>Interested in sponsorship opportunities?<br>
      Reach out to us at <a href="mailto:{{ site.email }}" target="_blank">{{ site.email }}</a></p>
    </div>
  </section>
</div>
