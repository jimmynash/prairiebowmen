---
title: Scheels Partnership
layout: landing
description: 'Membership Form and Dues Payment Options'
image:
nav-menu: false
show_tile: false
scheels_form: 'https://docs.google.com/forms/d/e/1FAIpQLSd0oyMFnxJzqQTbbJWoKmEbErmkD86MDFQ_6mbZZhsLi57hng/viewform?usp=sf_link'
published: false
---
<!-- Load Stripe.js on your website. -->
<script src="https://js.stripe.com/v3"></script>

<!-- Main -->
<div id="main">
  <div class="inner">
    <!-- trying again here -->
    <div class="row">
      <div class="4u 12u$(small)">
        <header class="major">
          <h2>Membership Form</h2>
          <p>Please fill out our membership form for Scheels team members.</p>
        </header>
        <ul class="actions vertical">
          <li><a href="{{page.scheels_form}}" target="_blank" class="button special icon fa-rocket">Membership Form</a></li>
        </ul>
      </div>
      <div class="4u 12u$(small)">
        <header class="major">
          <h2>Pay Online</h2>
          <p>Online payment through Stripe.com</p>
        </header>
        <div id="error-message"></div>
        <ul class="actions vertical">
          <!-- Single Membership -->
          <li>
          <button id="checkout-button-sku_GnrSH3zFdFV8I9" role="link" class="button special fit">Single Membership | $50/year</button>
          <script>
          (function() {
            var stripe = Stripe('pk_live_pRug873LI6xV4wjYZc5yMIsI00lQwbAuMM');

            var checkoutButton = document.getElementById('checkout-button-sku_GnrSH3zFdFV8I9');
            checkoutButton.addEventListener('click', function () {
              // When the customer clicks on the button, redirect
              // them to Checkout.
              stripe.redirectToCheckout({
                items: [{sku: 'sku_GnrSH3zFdFV8I9', quantity: 1}],

                // Do not rely on the redirect to the successUrl for fulfilling
                // purchases, customers may not always reach the success_url after
                // a successful payment.
                // Instead use one of the strategies described in
                // https://stripe.com/docs/payments/checkout/fulfillment
                successUrl: 'https://www.prairiebowmen.com/payment-complete',
                cancelUrl: 'https://www.prairiebowmen.com',
              })
              .then(function (result) {
                if (result.error) {
                  // If `redirectToCheckout` fails due to a browser or network
                  // error, display the localized error message to your customer.
                  var displayError = document.getElementById('error-message');
                  displayError.textContent = result.error.message;
                }
              });
            });
          })();
          </script>
          </li>
          <!-- end Single Membership -->
          <!-- Family Membership -->
          <li>
          <button id="checkout-button-sku_GnrSw1IToiwYpw" role="link" class="button special fit">Family Membership | $65/year</button>
          <script>
          (function() {
            var stripe = Stripe('pk_live_pRug873LI6xV4wjYZc5yMIsI00lQwbAuMM');

            var checkoutButton = document.getElementById('checkout-button-sku_GnrSw1IToiwYpw');
            checkoutButton.addEventListener('click', function () {
              // When the customer clicks on the button, redirect
              // them to Checkout.
              stripe.redirectToCheckout({
                items: [{sku: 'sku_GnrSw1IToiwYpw', quantity: 1}],

                // Do not rely on the redirect to the successUrl for fulfilling
                // purchases, customers may not always reach the success_url after
                // a successful payment.
                // Instead use one of the strategies described in
                // https://stripe.com/docs/payments/checkout/fulfillment
                successUrl: 'https://www.prairiebowmen.com/payment-complete',
                cancelUrl: 'https://www.prairiebowmen.com',
              })
              .then(function (result) {
                if (result.error) {
                  // If `redirectToCheckout` fails due to a browser or network
                  // error, display the localized error message to your customer.
                  var displayError = document.getElementById('error-message');
                  displayError.textContent = result.error.message;
                }
              });
            });
          })();
          </script>
          </li>
          <!-- end Family Membership -->
        </ul>
      </div>
      <div class="4u 12u$(small)">
        <header class="major">
          <h2>Pay via Check</h2>
          <p>Pay your dues by mailing a check to the following address:</p>
        </header>
        <p><pre>
          Prairie Bowmen Inc.
          3475 Woods Ave
          Lincoln NE 68510
        </pre></p>
      </div>
    </div>
  </div>
</div>
