---
layout: page
title: Newsletter Archive
description: "Prairie Bowment Newsletters"
image:
nav-menu: true
show_tile: false
order: 3

# example
#https://res.cloudinary.com/dgkbqzavd/image/upload/v1681170165/pba_media/newsletter/prairie_bowmen_newsletter_20230401_pwnulb.pdf
# https://res.cloudinary.com/dgkbqzavd/image/upload/{version}/{folder}/{filename}

newsletter_base: "https://res.cloudinary.com/dgkbqzavd/image/upload"
## Apparently I can't use variables in the array, I wanted to set these and then just reference them
## in the arrays below but it won't parse them out. Cloudinary changed the fucking version and now I have
## to account for it. Lame.

##

#version1: "v1577397129"
#version2: "v1681170165"
#folder: "pba_media/newsletter"

newsletters:
  - { title: 'Feb 2025 Newsletter', version: "v1739152009", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20250201_nkv1yv.pdf' }
  - { title: 'Jan 2025 Newsletter', version: "v1735868192", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20250101_di6ytj.pdf' }
  - { title: 'Dec 2024 Newsletter', version: "v1733448524", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20241201_wzzg4l.pdf' }
  - { title: 'Nov 2024 Newsletter', version: "v1731027218", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20241101_lygjoj.pdf' }
  - { title: 'Oct 2024 Newsletter', version: "v1728005194", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20241001_ml0bzb.pdf' }
  - { title: 'Sep 2024 Newsletter', version: "v1726013411", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240901_itk9bq.pdf' }
  - { title: 'Aug 2024 Newsletter', version: "v1722797813", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240801_kapoga.pdf' }
  - { title: 'July 2024 Newsletter', version: "v1720581512", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240701_ome027.pdf' }
  - { title: 'June 2024 Newsletter', version: "v1717979440", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240601_oyxvtq.pdf' }
  - { title: 'May 2024 Newsletter', version: "v1714949791", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240501_cp9bhm.pdf' }
  - { title: 'Apr 2024 Newsletter', version: "v1712421659", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240401_ibuds7.pdf' }
  - { title: 'Mar 2024 Newsletter', version: "v1710101622", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240301_bjpfd9.pdf' }
  - { title: 'Feb 2024 Newsletter', version: "v1707582722", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20240201_qa3ffo.pdf' }
  - { title: 'Jan 2024 Newsletter', version: "v1704681084", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_010724_dhfoqh.pdf' }
  - { title: 'Dec 2023 Newsletter', version: "v1701745238", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20231201_ezld0d.pdf' }
  - { title: 'Nov 2023 Newsletter', version: "v1699581614", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20231101_gvuefx.pdf' }
  - { title: 'Oct 2023 Newsletter', version: "v1696384284", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20231001_bxtzqj.pdf' }
  - { title: 'Sep 2023 Newsletter', version: "v1695174417", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230901_wpcfva.pdf' }
  - { title: 'Aug 2023 Newsletter', version: "v1692227885", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230801_c1hvic.pdf' }
  - { title: 'July 2023 Newsletter', version: "v1688870384", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230701_fzugbz.pdf' }
  - { title: 'June 2023 Newsletter', version: "v1685928002", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230601_dv8uh7.pdf' }
  - { title: 'May 2023 Newsletter', version: "v1681170165", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230501_slycea.pdf' }
  - { title: 'Apr 2023 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230401_pwnulb.pdf' }
  - { title: 'Mar 2023 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230301_el2o9w.pdf' }
  - { title: 'Feb 2023 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230201_mkm7ls.pdf' }
  - { title: 'Jan 2023 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20230102_ctt39o.pdf' }
  - { title: 'Dec 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20221201_h2hugr.pdf' }
  - { title: 'Nov 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20221101_crdvhw.pdf' }
  - { title: 'Oct 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20221001_mlmhza.pdf' }
  - { title: 'Sep 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220901_t64nwv.pdf' }
  - { title: 'Aug 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220801_wuceac.pdf' }
  - { title: 'July 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220701_b0g10o.pdf' }
  - { title: 'June 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220601_irvsma.pdf' }
  - { title: 'May 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220501_vrxqp0.pdf' }
  - { title: 'Apr 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220401_xusfjr.pdf' }
  - { title: 'Mar 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220301_pplebl.pdf' }
  - { title: 'Feb 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_20220201_u2iv6u.pdf' }
  - { title: 'Jan 2022 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_010322_yms6ic.pdf' }
  - { title: 'Dec 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_122021_oy3p5o.pdf' }
  - { title: 'Nov 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_112021_veph5r.pdf' }
  - { title: 'Oct 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_102021_cv3hvi.pdf' }
  - { title: 'Sep 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_092021_ov5byv.pdf' }
  - { title: 'Aug 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_082021_hzwwer.pdf' }
  - { title: 'July 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_072021_frodk0.pdf' }
  - { title: 'June 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_062021_mmzfpi.pdf' }
  - { title: 'May 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_052021_utnmy1.pdf' }
  - { title: 'Apr 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_042021_ojnt2w.pdf' }
  - { title: 'Mar 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_032021_vtqwwe.pdf' }
  - { title: 'Feb 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_022021_wyzmb6.pdf' }
  - { title: 'Jan 2021 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_012021_kbvouo.pdf' }
  - { title: 'Dec 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_122020_a22ezn.pdf' }
  - { title: 'Nov 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_112020_ofnv0j.pdf' }
  - { title: 'Oct 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_102020_gmllqn.pdf' }
  - { title: 'Sep 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_092020_ffht6p.pdf' }
  - { title: 'Aug 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_082020_ejy0va.pdf' }
  - { title: 'Jul 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_072020_zweyr9.pdf' }
  - { title: 'Jun 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_062020_wmum4o.pdf' }
  - { title: 'May 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_052020_bik4pl.pdf' }
  - { title: 'Apr 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_042020_r5i9oq.pdf' }
  - { title: 'Mar 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_032020_burx2y.pdf' }
  - { title: 'Feb 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'prairie_bowmen_newsletter_022020_vpj3xm.pdf' }
  - { title: 'Dec/Jan 2020 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Prairie_Bowmen_December-January_Newsletter_furzp8.pdf' }
  - { title: 'Aug 2019 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'aug2019newsletter_swzypb.pdf' }
  - { title: 'Jun 2019 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'June2019newsletter_x7frui.pdf' }
  - { title: 'Apr 2019 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'April2019Newsletter_iwfmqj.pdf' }
  - { title: 'Feb 2019 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'February2019Newsletterw_stqlz5.pdf' }
  - { title: 'Dec 2018 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'December2018Newsletter_r3nyd7.pdf' }
  - { title: 'Oct 2018 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'October2018Newsletter_y2vgbg.pdf' }
  - { title: 'Feb 2018 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Newsletter-February-2018_jyqk1c.pdf' }
  - { title: 'Dec 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Newsletter-December-2017_xzkkz6.pdf' }
  - { title: 'Oct 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'October2017Newsletter2_erxx4y.pdf' }
  - { title: 'Oct 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'October2017Newsletter_yqzxbh.pdf' }
  - { title: 'Aug 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'aug-2017-newsletter_fbbbj7.pdf' }
  - { title: 'Jun 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'June-2017-Newsletter_hcr1dx.pdf' }
  - { title: 'Apr 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'April-2017-Newsletter-no-combo_eowllc.pdf' }
  - { title: 'Apr 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'April-2017-Newsletter-combo_fql1lo.pdf' }
  - { title: 'Feb 2017 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'February-2017-Newsletter_xqnm2s.pdf' }
  - { title: 'Dec 2016 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'December-2016-Newsletter_zsaexc.pdf' }
  - { title: 'Jul 2016 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'July-2016-Newsletter_xtdhph.pdf' }
  - { title: 'May 2016 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'May-2016-Newsletter_ueg0qs.pdf' }
  - { title: 'Mar 2016 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Newsletter-MAR2016-NO-COMBO3_uudsua.pdf' }
  - { title: 'Jan 2016 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Newsletter-JAN2016_hd86xl.pdf' }
  - { title: 'Jul 2015 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'JUly2015Newsletter_nqn1dm.pdf' }
  - { title: 'Mar 2015 Newsletter 1', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Mar2015Newsletter1_sxgeyy.pdf' }
  - { title: 'Mar 2015 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Mar2015Newsletter_lgc27d.pdf' }
  - { title: 'Jan 2015 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Jan2015Newsletter_oywkwl.pdf' }
  - { title: 'Nov 2014 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'November-2014-Newsletter_o2qkij.pdf' }
  - { title: 'Sep 2014 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'September-2014-Newsletter_sou0fd.pdf' }
  - { title: 'Jul 2014 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'July-2014-Newsletter_pg3r8m.pdf' }
  - { title: 'May 2014 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'May-2014-Newsletter_ryqhnr.pdf' }
  - { title: 'Mar 2014 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'March-2014-newsletter_wwcs0y.pdf' }
  - { title: 'Jan 2014 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Jan-2014_shfn85.pdf' }
  - { title: 'Nov 2013 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Nov-2013-Newsletter_k7rkuk.pdf' }
  - { title: 'Sep 2013 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'Sept-2013-1_dma2zz.pdf' }
  - { title: 'Jul 2013 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'July-2013-Newsletter_f5drrl.pdf' }
  - { title: 'Mar 2013 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'March-2013-Newsletter_v3soqv.pdf' }
  - { title: 'Jan 2013 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'January-2013_gw28vz.pdf' }
  - { title: 'Nov 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'NOV2012PBNL_nnaaq0.pdf' }
  - { title: 'Aug 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'AUG2012PBNL_e4pdbq.pdf' }
  - { title: 'Jul 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'JULY2012PBNL_gi7wt7.pdf' }
  - { title: 'Jun 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'JUNE2012PBNL_ik1dek.pdf' }
  - { title: 'May 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'MAY2012PBNL_lymdfj.pdf' }
  - { title: 'Apr 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'APRIL2012PBNL_uqsnzw.pdf' }
  - { title: 'Mar 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'MAR2012PBNL_zq5nnr.pdf' }
  - { title: 'Feb 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'FEB2012PBNL-revised-DT1_afrxat.pdf' }
  - { title: 'Jan 2012 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'jan2012pbnl_a5xcsn.pdf' }
  - { title: 'Dec 2010 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'pbnldec2010_r2vp4t.pdf' }
  - { title: 'Nov 2010 Newsletter', version: "v1577397129", folder: "pba_media/newsletter", filename: 'nov2010pbnl_c3cwzm.pdf' }

---
<div id="main" class="alt">
  <section id="one">
    <div class="inner">
      <header class="major">
        <h1>{{ page.title }}</h1>
      </header>
      <div class="newsletters row">
        {% for nl in page.newsletters %}
          <article class="4u 12u$(medium) article special">
            <h2 class="title">{{ nl.title }}</h2>
            <a href="{{ page.newsletter_base }}/{{nl.version}}/{{nl.folder}}/{{ nl.filename }}" alt="Download {{ nl.title }}" title="{{ nl.title }}" target="_blank">
              <i class="icon fa-file-pdf-o fa-5x"></i></a>
              <hr>
          </article>
        {% endfor %}
      </div>
    </div>
  </section>
</div>
