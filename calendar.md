---
layout: landing
title: Events Calendar
description: "Prairie Bowmen Events Calendar"
image: assets/images/events_banner.jpg
nav-menu: true
order: 2
---
<a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@erothermel?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Eric Rothermel"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Image by Eric Rothermel</span></a>

<!-- main calendar - show for desktop -->
<section>
  <div class="inner">
    <div id="calendars">
      <div class="desktop-cal">
        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=America%2FChicago&src=prairiebowmenarchery%40gmail.com&ctz=America%2FChicago&color=%23DD4477&color=%236633CC&showTitle=0&showCalendars=1' style='border:solid 1px #777' width='800' height='600' frameborder='0' scrolling='no'></iframe></div>
      </div>
      <div class="mobile-cal">
        <style>.embed-container-mobile { position: relative; padding-bottom: 125%; height: 0; overflow: hidden; max-width: 100%; } .embed-container-mobile iframe, .embed-container-mobile object, .embed-container-mobile embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container-mobile'><iframe src='https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=America%2FChicago&src=prairiebowmenarchery%40gmail.com&ctz=America%2FChicago&color=%23DD4477&color=%236633CC&mode=AGENDA&showTitle=0' style='border:solid 1px #777' width='800' height='600' frameborder='0' scrolling='no'></iframe></div>
      </div>
    </div>
    <div class="container"><a href="https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=America%2FChicago&src=prairiebowmenarchery%40gmail.com&ctz=America%2FChicago&color=%23DD4477&color=%236633CC&showTitle=0&showCalendars=1" class="button special icon fa-calendar" target="_blank">Pop Out Calendar</a></div>
  </div>
</section>

