---
title: Get Started
layout: landing
description: 'You have to start somewhere right? Journey of a thousand steps and all that...'
image: assets/images/arrow_journey.jpg
nav-menu: false
nav-actions: true
nav-classes: 'button special fit'
order: 99
---

<!-- Main -->
<div id="main">

<!-- One -->
<section id="one">
	<div class="inner">
		<header class="major">
			<h2>Joining Prairie Bowmen</h2>
      <p>New members can apply using the button below. Renewing members are encouraged to fill out the renewal form.</p>
		</header>
    <div class="row">
      <div class="6u 12u$(medium)">
        <h4>Membership Opportunites</h4>
        <div class="table-wrapper">
          <table class="alt">
            <thead>
              <tr>
                <th>Type</th>
                <th>Description</th>
                <th>Dues</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Student/College</td>
                <td>For full time students aged 16-24 enrolled in college, trade school etc.
                  <br>Must provide student ID.</td>
                <td>$100/year</td>
              </tr>
              <tr>
                <td>Single</td>
                <td>For adults 19 years old and up.</td>
                <td>$130/year</td>
              </tr>
              <tr>
                <td>Family</td>
                <td>For two married adults and their children 19 and under<br>
                or one adult and his/her children 19 and under.</td>
                <td>$160/year</td>
              </tr>
            </tbody>
          </table>
        </div>
        <ul class="actions">
          <li><a href="{{ site.membership_google_form }}" class="button special icon fa-rocket" target="_blank">Apply Today</a></li>
          <li><a href="{{ site.membership_google_form }}" class="button special icon fa-clock-o" target="_blank">Renew Membership</a></li>
        </ul>
      </div>
      <div class="6u 12u$(medium)">
        <h4>New Members</h4>
        <p>What to expect when applying for new membership:</p>
        <div class="row">
          <div class="6u 12u$(small)">
            <h5>First Steps:</h5>
            <ul>
              <li>Fill out the <a href="{{ site.membership_google_form }}" target="_blank">application form.</a></li>
              <li><a href="/dues" target="_blank">Pay for the membership</a> you want.</li>
              <li><a href="/about#rules">Read the Rules and Regulations</a></li>
            </ul>
          </div>
          <div class="6u 12u$(small)">
            <h5>We will then:</h5>
            <ul>
              <li>Confirm your application and payment.</li>
              <li>Contact you with your membership details so you can get started.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <header class="major">
      <h2>Paying Dues</h2>
      <p>Dues are paid when applying for membership and annually on your membership join date.</p>
    </header>
    <ul class="actions">
      <li><a href="/dues" class="button special icon fa-dollar">Pay Dues</a></li>
    </ul>
	</div>
</section>

<!-- Two -->
<section id="two" class="spotlights">
	<section>
		<span class="image">
			<img src="/assets/images/youth_arrow.jpg" alt="" data-position="center center" />
		</span>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Junior Olympic Archery Development (JOAD)</h3>
				</header>
				<p>JOAD is a great way to get started with archery. Adults are welcome too!</p>
        <p>Offered in 10 week courses, $40/member or $50/non-member.</p>
        <p>Contact an instructor for more information:<br>
          Steve Woitaszewski: 402-450-3427<br>
          Harry Windle: 402-450-4276</p>
				<ul class="actions hide">
					<li><a href="generic.html" class="button">Learn more</a></li>
				</ul>
			</div>
		</div>
	</section>
	<section>
		<span class="image">
			<img src="/assets/images/target_league.jpg" alt="" data-position="top center" />
		</span>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Target Leagues</h3>
				</header>
				<p>Get together for some friendly competion with your fellow archers.</p>
        <p>Target League shoots happen on Monday nights at 7:00 p.m.<br>
        $5 per shoot (membership is not required).</p>
				<ul class="actions hide">
					<li><a href="generic.html" class="button">Learn more</a></li>
				</ul>
			</div>
		</div>
	</section>
	<section>
		<span class="image">
			<img src="/assets/images/target_shoots.jpg" alt="" data-position="25% 25%" />
		</span>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>3D Target Shoots</h3>
				</header>
				<p markdown="1">3D Target shoots happen on a regular basis.<br>
        $5 per shoot (membership is not required).</p>
        {% if site.facebook_url %}
        <p><a href="{{ site.facebook_url }}" target="_blank">Facebook</a> is the best place to check for up to date information.</p>
        {% endif %}
				<ul class="actions hide">
					<li><a href="generic.html" class="button">Learn more</a></li>
				</ul>
			</div>
		</div>
	</section>
</section>

</div>
