---
title: Contact Us
layout: landing
description: 'Get in touch. Our contact information is here.'
image: assets/images/contact_banner.jpg
nav-menu: true
order: 5
---

<!-- Main -->
<div id="main">
<a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@antoinebarres?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Antoine Barrès"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Image by Antoine Barrès</span></a>
<!-- One -->
<section id="one">
	<div class="inner">
		<header class="major">
			<h2>Contact Information</h2>
		</header>
		<div class="entry-content">
      <p><strong>Prairie Bowmen Archery Club</strong></p>
      <p>1432 N. Cotner<br>
        Lincoln, NE 68505<br>
      </p>
      <p><strong>Email:</strong> <a href="mailto:{{ site.email }}" target="_blank">{{ site.email }}</a><br>
      <strong>Phone:</strong> {{ site.phone }}
      </p>
      <p>Indoor range at 1432 N. Cotner</p>
    </div>
	</div>
</section>

<!-- Two -->
<section id="two" class="spotlights">
	<section>
		<a href="#" class="image">
			<img src="/assets/images/board_members_2022.jpg" alt="" data-position="center center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Board Members</h3>
				</header>
				<p>Current Board Members</p>
        <dl>
          <dt>PRESIDENT</dt>
          <dd>
            <p>JIM TUBBS – 402-430-7783 – <a href="mailto:jimtubbs@windstream.net" target="_blank">jimtubbs@windstream.net</a></p>
          </dd>
          <dt>VICE PRESIDENT / TARGET REP</dt>
          <dd>
            <p>CHAD MORGAN – 402-520-0343 – <a href="mailto:clmorgan72@gmail.com" target="_blank">clmorgan72@gmail.com</a></p>
          </dd>
          <dt>RANGE MASTER</dt>
          <dd>
            <p>HARRY WINDLE – 402-450-4276 – <a href="mailto:halwind.hw@gmail.com" target="_blank">halwind.hw@gmail.com</a></p>
          </dd>
          <dt>TREASURER/SECRETARY</dt>
          <dd>
            <p>KELLY SLAMA – 402-429-9562 – <a href="mailto:prairiekellycc@gmail.com" target="_blank">prairiekellycc@gmail.com</a></p>
          </dd>
          <dt>BOWHUNTER REP</dt>
          <dd>
            <p>DANNY TUBBS – 402-525-3695 – <a href="mailto:dtubbs402@gmail.com" target="_blank">dtubbs402@gmail.com</a></p>
          </dd>
          <dt>COMMUNICATIONS DIRECTOR</dt>
          <dd>
            <p>CAUGHLIN BOHN – <a href="mailto:cbohn.prairie@gmail.com" target="_blank">cbohn.prairie@gmail.com</a></p>
          </dd>
          <dt>YOUTH COORDINATOR</dt>
          <dd>
            <p>STEVE WOITASZEWSKI – 402-450-3427 –  <a href="mailto:swoit@allophone.com" target="_blank">swoit@allophone.com</a></p>
          </dd>
        </dl>
			</div>
		</div>
	</section>
</section>
</div>
