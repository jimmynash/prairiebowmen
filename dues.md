---
title: Pay Dues
layout: landing
description: 'Need to get paid up?'
image:
nav-menu: true
show_tile: false
order: 6
---
<!-- Load Stripe.js on your website. -->
<script src="https://js.stripe.com/v3"></script>

<!-- Main -->
<div id="main">
  <div class="inner">
    <!-- trying again here -->
    <div class="row">
      <div class="6u 12u$(small)">
        <header class="major">
          <h2>Pay Online</h2>
          <p>Online payment through Stripe.com</p>
        </header>
        <div id="error-message"></div>
        <ul class="actions vertical">
          <!-- Student/College -->
          <li>
          <button id="checkout-button-sku_GfFU8ckxkTYBwv" role="link" class="button special fit">Student Membership | $105/year</button>
          <script>
          (function() {
            var stripe = Stripe('pk_live_pRug873LI6xV4wjYZc5yMIsI00lQwbAuMM');

            var checkoutButton = document.getElementById('checkout-button-sku_GfFU8ckxkTYBwv');
            checkoutButton.addEventListener('click', function () {
              // When the customer clicks on the button, redirect
              // them to Checkout.
              stripe.redirectToCheckout({
                items: [{sku: 'sku_GfFU8ckxkTYBwv', quantity: 1}],

                // Do not rely on the redirect to the successUrl for fulfilling
                // purchases, customers may not always reach the success_url after
                // a successful payment.
                // Instead use one of the strategies described in
                // https://stripe.com/docs/payments/checkout/fulfillment
                successUrl: 'https://www.prairiebowmen.com/payment-complete',
                cancelUrl: 'https://www.prairiebowmen.com',
              })
              .then(function (result) {
                if (result.error) {
                  // If `redirectToCheckout` fails due to a browser or network
                  // error, display the localized error message to your customer.
                  var displayError = document.getElementById('error-message');
                  displayError.textContent = result.error.message;
                }
              });
            });
          })();
          </script>
          <p><i>For full time students aged 16-24 enrolled in college, trade school etc. <br>Must provide student ID.</i></p>
          </li>
          <!-- end student/college script -->
          <!-- Single Membership -->
          <li>
          <button id="checkout-button-sku_GfFVj3w92jPvjX" role="link" class="button special fit">Single Membership | $135/year</button>
          <script>
          (function() {
            var stripe = Stripe('pk_live_pRug873LI6xV4wjYZc5yMIsI00lQwbAuMM');

            var checkoutButton = document.getElementById('checkout-button-sku_GfFVj3w92jPvjX');
            checkoutButton.addEventListener('click', function () {
              // When the customer clicks on the button, redirect
              // them to Checkout.
              stripe.redirectToCheckout({
                items: [{sku: 'sku_GfFVj3w92jPvjX', quantity: 1}],

                // Do not rely on the redirect to the successUrl for fulfilling
                // purchases, customers may not always reach the success_url after
                // a successful payment.
                // Instead use one of the strategies described in
                // https://stripe.com/docs/payments/checkout/fulfillment
                successUrl: 'https://www.prairiebowmen.com/payment-complete',
                cancelUrl: 'https://www.prairiebowmen.com',
              })
              .then(function (result) {
                if (result.error) {
                  // If `redirectToCheckout` fails due to a browser or network
                  // error, display the localized error message to your customer.
                  var displayError = document.getElementById('error-message');
                  displayError.textContent = result.error.message;
                }
              });
            });
          })();
          </script>
          <p><i>For adults 19 years old and up.</i></p>
          </li>
          <!-- end Single Membership -->
          <!-- Family Membership -->
          <li>
          <button id="checkout-button-sku_GfFVSLAAOekAWg" role="link" class="button special fit">Family Membership | $165/year</button>
          <script>
          (function() {
            var stripe = Stripe('pk_live_pRug873LI6xV4wjYZc5yMIsI00lQwbAuMM');

            var checkoutButton = document.getElementById('checkout-button-sku_GfFVSLAAOekAWg');
            checkoutButton.addEventListener('click', function () {
              // When the customer clicks on the button, redirect
              // them to Checkout.
              stripe.redirectToCheckout({
                items: [{sku: 'sku_GfFVSLAAOekAWg', quantity: 1}],

                // Do not rely on the redirect to the successUrl for fulfilling
                // purchases, customers may not always reach the success_url after
                // a successful payment.
                // Instead use one of the strategies described in
                // https://stripe.com/docs/payments/checkout/fulfillment
                successUrl: 'https://www.prairiebowmen.com/payment-complete',
                cancelUrl: 'https://www.prairiebowmen.com',
              })
              .then(function (result) {
                if (result.error) {
                  // If `redirectToCheckout` fails due to a browser or network
                  // error, display the localized error message to your customer.
                  var displayError = document.getElementById('error-message');
                  displayError.textContent = result.error.message;
                }
              });
            });
          })();
          </script>
          <p><i>For two married adults and their children 19 and under <br>or one adult and his/her children 19 and under.</i></p>
          </li>
          <!-- end Family Membership -->
        </ul>
      </div>
      <div class="6u 12u$(small)">
        <header class="major">
          <h2>Pay via Check</h2>
          <p>Pay your dues by mailing a check to the following address:</p>
        </header>
        <p><pre>
          Prairie Bowmen Inc.
          3475 Woods Ave
          Lincoln NE 68510
        </pre></p>
      </div>
    </div>
  </div>
</div>
