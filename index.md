---
layout: home
title: Home
landing-title: 'We <i class="fa fa-heart-o"></i> Archery.'
description: Join us.
image: null
author: null
show_tile: false
order: 0
---

<div id="home-bottom" class="row top-pad-20 inner">
  <section class="6u 12u$(small) special">
      <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FPrairie-Bowmen-Archery-Lincoln-104249611115990%2F&tabs=timeline&width=800&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=188487257844564" width="800" height="500" style="border:none;overflow:hidden;margin:0px auto;width:500px;" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
    <ul class="icons">
      {% if site.facebook_url %}
        <li><a href="{{ site.facebook_url }}" class="icon alt fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
      {% endif %}
    </ul>
  </section>
  <section class="6u$ 12u$(small) special">
    <a class="twitter-timeline" data-theme="dark" data-height="500" data-width="500" href="https://twitter.com/pbarchery?ref_src=twsrc%5Etfw">Tweets by pbarchery</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    <ul class="icons">
      {% if site.twitter_url %}
        <li><a href="{{ site.twitter_url }}" class="icon alt fa-twitter" target="_blank"><span class="label">Twitter</span></a></li>
      {% endif %}
    </ul>
  </section>
</div>
