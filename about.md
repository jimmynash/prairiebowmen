---
layout: landing
title: About | Rules | Regulations
description: "Club History, Rules and Regulations"
image: assets/images/wall_arrows.jpg
nav-menu: true
order: 1
---
<div id="main">
  <section id="history">
    <div class="inner">
      <h2>History</h2>
      <p>Founded in 1954, Praire Bowmen Inc. is home to archery enthusiasts of all ages and skills. Archery is a sport that anyone can enjoy so we provide several options for youth, adults and families.</p>
      <p>Club members enjoy the benefits of our private 24/7 access to our indoor range.</p>
      <p>We are located in East Central Lincoln at 1432 N. Cotner Blvd.</p>
      {% if false %}
      <p>The ranges can be used for daily shooting, birthdays, family gatherings, boy scouts, etc. All we ask is that you get board permission and pay a $50 good will donation to the club. If you would like to use the clubhouse we do ask for a $100 deposit.</p>
      {% endif %}
      <p>Prairie Bowmen Inc. offers many events throughout the year for members. We have winter and summer Target and 3-D Leagues and Tournaments, Junior Olympic Archery Development, Adult Archery Achievement, annual Jamboree and annual Banquet.</p>
      <p>Prairie Bowmen Inc. is affiliated with several national archery organizations including NFAA, FITA and USA Archery. Throughout the year we host several events that bring archers from around the region to compete.</p>
    </div>
  </section>
  <section id="rules">
    <div class="inner">
      <h2>Rules and Regulations</h2>
      <section id="general-rules">
        <header class="major">
          <h3>General</h3>
        </header>
        <p>Pay dues promptly to avoid being dropped or charged with a late fee. All dues are due on your anniversary month.</p>
        <p>After paying 20 years of continuous dues (not counting the first year) and in <b>GOOD STANDING</b> during that whole period you will become a life member.</p>
        <p>Members are allowed to bring a shooting guest only twice to the ranges, after that the guest is then expected to join the Club. Spectators are always welcomed.</p>
        <p>Prairie Bowmen Archery Club is run by volunteers only, the help of our members is always welcomed and appreciated. <b>GET INVOLVED</b> and make Prairie Bowmen a club you can be proud to say you are a member of!</p>
      </section>
      <section id="indoor-rules">
        <header class="major">
          <h3>Indoor Range</h3>
        </header>
        <ul>
          <li>Open to current members 24/7.</li>
          <li>Light switch is located on south wall as you enter (above table).</li>
          <li>PLEASE make sure all lights are off if you are the last to leave.</li>
          <li>Thermostats – winter months, do not set higher than 68.</li>
          <li>Summer months do not set lower than 78.</li>
          <li>Always turn down to 50, if you are the last to leave.</li>
          <li>Lockers are available for rent at $50 per year or $30 for 6 months.</li>
          <li>Paper targets are available for 50 cents (put money in mail slot by office door).</li>
          <li>Everyone is responsible for cleaning up after themselves.</li>
          <li>Members may bring a guest for $10.00. Envelopes will be provided. It is the members responsibility to collect the money and deposit in the mail slot, next to the office door.</li>
        </ul>
        <hr>
        <ul>
          <li><b>NO</b> illegal or controlled substances.</li>
          <li><b>NO</b> firearms of any kind.</li>
          <li><b>NO</b> throwing weapons or objects of any kind.</li>
          <li>Crossbows may be shot only at designated target.</li>
          <li><b>NO BROADHEADS</b> of any kind are allowed in the range at any time.</li>
          <li><b>NO</b> outsert tips of any kind.</li>
          <li>Anything that shoots a projectile other than archery equipment is prohibited.</li>
          <li><b>NO</b> unattended children. (An adult must accompany children 16 and under at all times)</li>
          <li><b>NO</b> horseplay or unauthorized games.</li>
          <li><b>NO</b> pets.</li>
        </ul>
        <ul class="alt">
          <li><b>Make sure lights are off, heat turned down and door locked when you leave.</b></li>
        </ul>
      </section>
    </div>
  </section>
</div>
